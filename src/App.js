import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Homepage = React.lazy(()=>import('./views/Layout/Home'));
const Instructors = React.lazy(()=>import('./views/Instructors/Instructors'));
const Classes = React.lazy(()=>import('./views/Classes/Classes'));
const Bookings = React.lazy(()=>import('./views/Bookings/Bookings'));

const App = () => {
  const loading = () => {
    return(
      <div className="d-flex flex-column justify-content-center align-items-center vh-100">
        <Loader
            type="Triangle"
            color="#000"
            height={100}
            width={100}
            timeout={30000}
          />
          Loading...
      </div>
    )
  }


  return(
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props}/>}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route 
            path="/"
            exact
            name="Homepage"
            render={props => <Homepage {...props}/>}
          />
          <Route 
            path="/instructors"
            exact
            name="Instructors"
            render={props => <Instructors {...props}/>}
          />
          <Route 
            path="/classes"
            exact
            name="Classes"
            render={props => <Classes {...props}/>}
          />
          <Route 
            path="/allbookings"
            exact
            name="Bookings"
            render={props => <Bookings {...props}/>}
          />
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;
