import React, {useState} from 'react';
import {
  CardTitle,
  Button,
  Card,
  CardBody
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import {
  FormInput
} from '../../globalcomponents';
import Axios from 'axios';
import Swal from 'sweetalert2';

const Login = () => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [emailRequired, setEmailRequired] = useState(true)
  const [passwordRequired, setPasswordRequired] = useState(true)

  const handleEmailChange = (e) => {
    if(e.target.value === ""){
      setEmailRequired(true)
      setEmail("")
    }else{
      setEmailRequired(false)
      setEmail(e.target.value)
    }
  }

  const handlePasswordChange = (e) => {
    if(e.target.value === ""){
      setPasswordRequired(true)
      setPassword("")
    }else{
      setPasswordRequired(false)
      setPassword(e.target.value)
    }
  }

  const handleLogin = () => {
    if(emailRequired){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Email Required!',
      })
    }else if(passwordRequired){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Password Required!',
      })
    }else{
      Axios.post('https://radiant-forest-82895.herokuapp.com/login', {
        email,
        password
      }).then(res=>{
        sessionStorage.token = res.data.token;
        sessionStorage.user = JSON.stringify(res.data.user);
        window.location.replace('/');
      }).catch(()=>{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Email or Password not match!',
        })
      })
    }
  }

  return(
    <React.Fragment>
      <div className="row main-page right-side-img">
        <div className="col-lg-6 left-side-login">
          <div className="col-lg-10 offset-lg-1 py-5">
            <Card className="my-5">
              <CardBody>
                <CardTitle className="text-center"><h1>Login</h1></CardTitle>
                <FormInput
                  label={"Email"}
                  type={"email"}
                  name={"email"}
                  placeholder={"Enter Email"}
                  onChange={handleEmailChange}
                  required={emailRequired}
                />
                <FormInput
                  label={"Password"}
                  type={"password"}
                  name={"password"}
                  placeholder={"Enter Password"}
                  onChange={handlePasswordChange}
                  required={passwordRequired}
                />
                <Button
                  className="btn-default"
                  block
                  onClick={handleLogin}
                >Login</Button>
                <div className="pt-3  ">
                  <label>Don't have an account? <Link to="/register"><strong>Register</strong></Link></label>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Login;