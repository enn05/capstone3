import React, {useState} from 'react';
import {
  Row,
  Col,
  CardTitle,
  Button,
  Card,
  CardBody,
} from 'reactstrap';
import {
  Link
} from 'react-router-dom';
import {
  FormInput
} from '../../globalcomponents';
import Axios from 'axios';
import Swal from 'sweetalert2';

const success = () => {
  Swal.fire(
    'Success!',
    'You are now registered',
    'success'
  )
}

const loading = () => {
  return(
    <div className="d-flex flex-column justify-content-center align-items-center vh-100">
      <Loader
          type="Triangle"
          color="#000"
          height={100}
          width={100}
          timeout={5000}
        />
        Loading...
    </div>
  )
}

const Register = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirm, setConfirm] = useState("");
  const [nameRequired, setNameRequired] = useState(true);
  const [emailRequired, setEmailRequired] = useState(true);
  const [passwordRequired, setPasswordRequired] = useState(true);
  const [confirmRequired, setConfirmRequired] = useState(true);
  const [notMatched, setNotMatched] = useState(true);

  const handleEmailChange = (e) => {
    if(e.target.value === ""){
      setEmailRequired(true)
      setEmail("")
    }else{
      setEmailRequired(false)
      setEmail(e.target.value)
    }
  }

  const handleNameChange = (e) => {
    if(e.target.value === ""){
      setNameRequired(true)
      setName("")
    }else{
      setNameRequired(false)
      setName(e.target.value)
    }
  }

  const handlePasswordChange = (e) => {
    if(e.target.value === ""){
      setPasswordRequired(true)
      setPassword("")
    }else{
      setPasswordRequired(false)
      setPassword(e.target.value)
    }
  }

  const handlePasswordConfirm = (e) => {
    if(e.target.value === ""){
      setConfirmRequired(true)
      setConfirm("")
    }else{
      setConfirmRequired(false)
      setConfirm(e.target.value)
    }
  }

  const handleRegister = (e) => {
    if(emailRequired){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Email Required!',
      })
    }else if(nameRequired){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Name Required!',
      })
    }else if(passwordRequired){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Password Required!',
      })
    }else if(password !== confirm){
      setNotMatched(true)
      console.log('password did not match')
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Passwords did not match!',
      })
    }else{
      setNotMatched(false)
      Axios.post('https://radiant-forest-82895.herokuapp.com/register', {
        name,
        email,
        password
      }).then(res=>{
        loading()
        success()
        window.location.replace('#/login');
      })
    }
  }

  return(
    <React.Fragment>
        <Row className="main-page">
          <Col className="left-side"></Col>
          <Col className="right-side d-flex- justify-content-center align-items-center">
            <div className="col-lg-10 offset-lg-1 py-3">
              <Card className="my-5">
                <CardBody>
                  <CardTitle className="text-center"><h1>Register</h1></CardTitle>
                  <FormInput 
                    label={"Email"}
                    type={"email"}
                    name={"email"}
                    placeholder={"Enter Email"}
                    onChange={handleEmailChange}
                    required={emailRequired}
                  />
                  <FormInput 
                    label={"Fullname"}
                    type={"text"}
                    name={"name"}
                    placeholder={"Enter Fullname"}
                    onChange={handleNameChange}
                    required={nameRequired}
                  />
                  <FormInput 
                    label={"Password"}
                    type={"password"}
                    name={"password"}
                    placeholder={"Enter Password"}
                    onChange={handlePasswordChange}
                    required={passwordRequired}
                    notMatched={notMatched}
                  />
                  <FormInput 
                    label={"Confirm Password"}
                    type={"password"}
                    name={"confirm"}
                    placeholder={"Enter Password"}
                    onChange={handlePasswordConfirm}
                    required={confirmRequired}
                    notMatched={notMatched}
                  />
                  <Button 
                    className="btn-default"
                    onClick={handleRegister}
                    // disabled={emailRequired || nameRequired || passwordRequired || confirmRequired ? true : false}
                    block
                  >Register</Button>
                  <div className="pt-3  ">
                    <label>Already have an account? <Link to="/login"><strong>Login</strong></Link></label>
                  </div>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
    </React.Fragment>
  )
}

export default Register;