import React from "react";

const BookingRow = props => {
  const booking = props.booking;
  return (
    <React.Fragment>
      <tr>
        <td>{booking.transactionCode}</td>
        <td>{booking.transactionDate}</td>
        <td>{booking.className}</td>
        <td>
          {booking.dates.map(dateList=>{
              return dateList + ',\n'
            })
          }
        </td>
        {props.user.isAdmin
          ?
          <td>{booking.userName}</td>
          : ""
        }
        <td>{booking.totalPayment}</td>
      </tr>
    </React.Fragment>
  );
};

export default BookingRow;
