import React, {useEffect, useState} from 'react'
import Navbar from '../Layout/Navbar'
import { Table } from 'reactstrap'
import BookingRow from './BookingRow'
import axios from 'axios'
import { CSVLink } from 'react-csv'

const Bookings = () => {
  const [bookings, setBookings] = useState([])
  const csvData = bookings
  const headers = [
    { label: "Transaction #", key: "transactionCode" },
    { label: "Date", key: "transactionDate" },
    { label: "Class", key: "className" },
    { label: "Booked Date(s)", key: "dates" },
    { label: "User", key: "userName" },
    { label: "Amount", key: "totalPayment" },
  ]
  const [user, setUser] = useState("")

  useEffect(() => {
    if (sessionStorage.token) {
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
      if (user.isAdmin) {
        axios.get("https://radiant-forest-82895.herokuapp.com/showbookings/")
          .then(res => setBookings(res.data));
      }else{
        axios.get("https://radiant-forest-82895.herokuapp.com/showbookingsbyuserid/" + user.id)
          .then(res => setBookings(res.data));
      }
    }
  }, []);

  return (
    <React.Fragment>
      <Navbar />
      <div className="pt-5">
        <h1 className="text-center pt-5 pb-3" id="border-design">Bookings</h1>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-12">
              <div className="card">
                <div className="card-header">
                  <div className="row">
                    <div className="col-4 offset-8 text-right">
                      {user.isAdmin
                        ?
                        <CSVLink data={csvData} headers={headers} className="btn btn-secondary">Export to CSV</CSVLink>
                        : ""
                      }
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  <Table responsive bordered hover className="table" size="sm">
                    <thead>
                      <tr>
                        <th>Transaction #</th>
                        <th>Date</th>
                        <th>Class</th>
                        <th>Booked Date(s)</th>
                        {user.isAdmin
                          ?
                          <th>User</th>
                          : ""
                        }
                        <th>Total Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      {bookings.map(booking=>
                        <BookingRow 
                          key={booking._id}
                          booking={booking}
                          user={user}
                        />
                      )}
                    </tbody>
                  </Table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default Bookings;