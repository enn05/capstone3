import React, {useState} from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import axios from 'axios';
import {
	Button, Label
}from "reactstrap";
import Swal from 'sweetalert2';
import moment from 'moment';

const success = () => {
  Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Booking Success!',
		showConfirmButton: false,
		timer: 500
	})
}

const CheckoutForm = (props) => {

	const [stripeRequired, setStripeRequired] = useState(false)

	const submit = async (e) => {
		let transactionCode = moment(new Date()).format("x")
		let total = props.totalAmount * 100
		let { token } = await props.stripe.createToken({
			name: "Name"
		})
		if(!sessionStorage.token){
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Your are not logged in!',
			}).then((result) => {
				window.location.replace('#/login')
			})
		}else{
			await axios({
				method: "POST",
				url: 'https://radiant-forest-82895.herokuapp.com/addbooking',
				data: {
					userName: props.user,
					userId: props.userId,
					className: props.cName,
					classId: props.cId,
					dates: props.selectedDays,
					totalPayment: props.totalAmount,
					transactionCode: transactionCode
				}
			}).then(res=>{
				if(res.status === 200){
					axios.post('https://radiant-forest-82895.herokuapp.com/charge', {
						amount: total,
							source: token.id
					}).then(res => {
						if(res.data.status === "succeeded"){
							success()
							window.location.reload()
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Oops...',
								text: 'Something went wrong!',
							})
						}
					})
				}
			}).catch(()=>{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong! Please try again',
        })
      })
		}
	}

	const handleonChange = (e) => {
		if(e.complete === false){
			setStripeRequired(false)
		}else {
			setStripeRequired(true)
		}

	}

	return (
		<div className="container text-center bg-payment py-3">
			<Label><strong>Total Amount: {props.totalAmount}</strong></Label>
			<div className="checkout">
				<p>Would you like to complete booking?</p>
				<CardElement 
					style={{base: {color: 'white'}}}
					onChange={handleonChange}
				/>
				<br />
				<Button
					disabled={props.totalAmount === 0 || stripeRequired === false ? true : false}
					block
					onClick={submit}
					className="stp-btn"
				>Book</Button>
			</div>
		</div>
	)
}

export default injectStripe(CheckoutForm);