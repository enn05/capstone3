import InstructorRow from './InstructorsRow';
import InstructorForm from './InstructorForm';

export{
  InstructorRow,
  InstructorForm
}