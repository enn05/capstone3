import React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button
} from 'reactstrap';
import {
  FormInput
} from '../../../globalcomponents';

const InstructorFrom = ({showForm,handleShowForm,handleInstructorName,handelInstructorEmail,handleInstructorGenre,...props}) => {
  return(
    <React.Fragment>
      <Modal
        isOpen={showForm}
        toggle={handleShowForm}
        >
        <ModalHeader
          toggle={handleShowForm}
          // style={{"backgroundColor": "black", "color": "white"}}
        >
          Add Instructor
        </ModalHeader>
        <ModalBody>
          <FormInput 
            label={"Name"}
            type={"text"}
            name={"name"}
            placeholder={"Enter Instructor Name"}
            required={props.nameRequired}
            onChange={handleInstructorName}
          />
          <FormInput 
            label={"Email"}
            type={"email"}
            name={"email"}
            placeholder={"Enter Instructor Email"}
            required={props.emailRequired}
            onChange={handelInstructorEmail}
          />
          <FormInput 
            label={"Genre"}
            type={"text"}
            name={"genre"}
            placeholder={"Enter Dance Genre"}
            required={props.genreRequired}
            onChange={handleInstructorGenre}
          />
          <Button
            block
            disabled={props.name !== "" && props.email !== "" && props.genre !== "" ? false : true}
            onClick={props.handleSave}
          >Add Instructor</Button>
        </ModalBody>
      </Modal>
    </React.Fragment>
  )
}

export default InstructorFrom;