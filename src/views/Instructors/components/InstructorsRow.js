import React from 'react';
import {
  Button,
} from 'reactstrap';

const InstructorRow = (props) => {
  const instructor = props.instructor
  return(
    <React.Fragment>
      <tr>
        <td>{instructor.name}</td>
        <td>{instructor.email}</td>
        <td>{instructor.genre}</td>
        <td>
        {props.user.isAdmin ?
          <Button
            color="danger"
            onClick={()=>props.handleDeleteInstructor(instructor._id)}
          >Delete</Button>
          : ""
        }
        </td>
      </tr>
    </React.Fragment>
  )
}

export default InstructorRow;