import React, {useState, useEffect} from 'react';
import {
  InstructorRow,
  InstructorForm
} from './components';
import { Table } from 'reactstrap';
import Navbar from '../Layout/Navbar';
import Swal from 'sweetalert2';


const Instructors = () => {
  const [instructors, setInstructors] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [genre, setGenre] = useState("");
  const [nameRequired, setNameRequired] = useState(true);
  const [emailRequired, setEmailRequired] = useState(true);
  const [genreRequired, setGenreRequired] = useState(true);
  const [user, setUser] = useState({});


  useEffect(()=>{
    fetch('https://radiant-forest-82895.herokuapp.com/showinstructors')
      .then(res => res.json())
      .then(data => {
        setInstructors(data)
      });
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])

  // setUser(JSON.parse(sessionStorage.user))

  const handleRefresh = () => {
    setShowForm(false)
    setName("")
    setEmail("")
    setGenre("")
    setNameRequired(true)
    setEmailRequired(true)
    setGenreRequired(true)
  }

  const handleShowForm = () => {
    setShowForm(!showForm)
  }

  const handleInstructorName = (e) => {
    if(e.target.value === ""){
      setNameRequired(true)
      setName("")
    }else{
      setNameRequired(false)
      setName(e.target.value)
    }
  }

  const handelInstructorEmail = (e) => {
    if(e.target.value === ""){
      setEmailRequired(true)
      setEmail("")
    }else{
      setEmailRequired(false)
      setEmail(e.target.value)
    }
  }

  const handleInstructorGenre = (e) => {
    if(e.target.value === ""){
      setGenreRequired(true)
      setGenre("")
    }else{
      setGenreRequired(false)
      setGenre(e.target.value)
    }
  }

  const handleSave = () =>{
    fetch('https://radiant-forest-82895.herokuapp.com/addinstructor', {
      method: 'post',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        name: name,
        email: email,
        genre: genre
      })
    })
      .then(res => res.json())
      .then(data => {
        let newInstructors = [...instructors];
        newInstructors.push(data)
        setInstructors(newInstructors);
        Swal.fire(
          'Success!',
          'Instructor added successfully',
          'success'
        )
      })
    handleRefresh();
  }

  const handleDeleteInstructor = (instructorId) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        fetch('http://localhost:4000/deleteinstructor/' + instructorId, {
          method: 'delete'
        }).then(res => {
          let newInstructors = instructors.filter(instructor=>instructor._id !== instructorId)
          setInstructors(newInstructors)
        })
      }
    })
  }

  return(
    <React.Fragment>
      <Navbar />
        <div className="pt-5">
          <h1 className="text-center py-5" id="border-design"><span></span> Instructors <span></span></h1>
          <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <div className="row">
                        <div className="col-4 offset-8 text-right">
                        {user.isAdmin ?
                          <button className="btn btn-sm btn-secondary"
                            onClick={handleShowForm}
                          >Add Instructor</button>
                          : ""
                        }
                          <InstructorForm 
                            showForm={showForm}
                            handleShowForm={handleShowForm}
                            handleInstructorName={handleInstructorName}
                            handelInstructorEmail={handelInstructorEmail}
                            handleInstructorGenre={handleInstructorGenre}
                            nameRequired={nameRequired}
                            emailRequired={emailRequired}
                            genreRequired={genreRequired}
                            name={name}
                            email={email}
                            genre={genre}
                            handleSave={handleSave}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <Table responsive bordered hover className="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Genre</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {instructors.map(instructor=>
                            <InstructorRow 
                              key={instructor._id}
                              instructor={instructor}
                              handleDeleteInstructor={handleDeleteInstructor}
                              user={user}
                            />
                            )}
                        </tbody>
                      </Table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </React.Fragment>
  )
}

export default Instructors