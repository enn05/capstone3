import React, {useEffect} from 'react';
import Navbar from './Navbar';
import Swal from 'sweetalert2';

const Home = () => {

  useEffect(() => {
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      Swal.fire(
        `Welcome ${user.name}!`,
        'You\'re logged in'
      )
    }
  }, [])

  return(
    <React.Fragment>
      <Navbar />
      <div className="home">
      </div>
    </React.Fragment>
  )
}

export default Home;