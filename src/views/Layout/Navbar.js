import React, {useEffect, useState} from 'react';
import {
  Link
} from 'react-router-dom';
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

const Navbar = ({className,...props}) => {
  const [user, setUser] = useState("")
  useEffect(() => {
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])

  const handleLogout = () => {
    sessionStorage.clear()
    window.location.replace('#/login')
  }

  return(
    <React.Fragment>
      <nav className="navbar navbar-sticky-top navbar-expand-lg navbar-dark nav-bg-dark ds-nav">
        <div className="container">
          <Link className="navbar-brand" to="/">DanceStyler</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <i className="fas fa-bars"></i>
          </button>

          <div className="collapse navbar-collapse" id="navbarColor03">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/classes">Classes</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/instructors">Instructors</Link>
              </li>
              {user
                ?
                <li className="nav-item">
                  <UncontrolledDropdown setActiveFromChild>
                    <DropdownToggle tag="a" className="nav-link" caret>
                      {user.name}
                    </DropdownToggle>
                    <DropdownMenu>
                    {user.isAdmin 
                      ? 
                      <DropdownItem
                        href="#/allbookings"
                      >Management</DropdownItem>
                      :
                      <DropdownItem
                        href="#/allbookings"
                      >My Bookings</DropdownItem>
                    }
                      <DropdownItem
                        onClick={handleLogout}
                      >Logout</DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </li>
                :
                <React.Fragment>
                  <li className="nav-item">
                    <Link className="nav-link" to="/register">Register</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/login">Login</Link>
                  </li>
                </React.Fragment>
              }
            </ul>
          </div>
        </div>
      </nav>
    </React.Fragment>
  )
}

export default Navbar;

// <nav className="navbar navbar-sticky-top navbar-expand-lg navbar-dark nav-bg-dark ds-nav"></nav>