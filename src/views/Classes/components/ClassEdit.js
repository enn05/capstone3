import React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button
} from 'reactstrap';
import {
  FormInput
} from '../../../globalcomponents';

const ClassEdit = ({showForm,handleShowForm,handleClassName,handelInstructor,handleClassSchedule,handleClassTime,handleClassRate,...props}) => {
  console.log(props.classes.className)
  return(
    <React.Fragment>
      <Modal
        isOpen={showForm}
        toggle={handleShowForm}
        >
        <ModalHeader
          toggle={handleShowForm}
          // style={{"backgroundColor": "black", "color": "white"}}
        >
          Edit {props.classes.className} Class
        </ModalHeader>
        <ModalBody>
          <FormInput 
            label={"Class Name"}
            type={"text"}
            name={"className"}
            placeholder={"Enter Class Name"}
            required={props.classNameRequired}
            onChange={handleClassName}
            defaultValue={props.classes.className}
          />
          <FormInput 
            label={"Instructor"}
            type={"text"}
            name={"instructor"}
            placeholder={"Enter Instructor Name"}
            required={props.instructorRequired}
            onChange={handelInstructor}
            defaultValue={props.classes.instructor}
          />
          <FormInput 
            label={"Schedule"}
            type={"text"}
            name={"schedule"}
            placeholder={"Enter Class Schedule"}
            required={props.scheduleRequired}
            onChange={handleClassSchedule}
            defaultValue={props.classes.schedule}
          />
          <FormInput 
            label={"Time"}
            type={"text"}
            name={"time"}
            placeholder={"Enter Time Slot"}
            required={props.timeRequired}
            onChange={handleClassTime}
            defaultValue={props.classes.time}
          />
          <FormInput 
            label={"Rate"}
            type={"number"}
            name={"rate"}
            placeholder={"Enter Class Rate"}
            required={props.rateRequired}
            onChange={handleClassRate}
            defaultValue={props.classes.rate}
          />
          <input
            className="form-control"
            type="file"
            accept="image/*"
            onChange={(e) => props.handleClassImageChange(e)}
          />
          <br />
          <Button
            block
            onClick={props.handleSave}
          >Edit Class</Button>
        </ModalBody>
      </Modal>
    </React.Fragment>
  )
}

export default ClassEdit;