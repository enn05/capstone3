import React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  Button
} from 'reactstrap';
import {
  FormInput
} from '../../../globalcomponents';

const ClassFrom = ({showForm,handleShowForm,handleClassName,handelInstructor,handleClassSchedule,handleClassTime,handleClassRate,...props}) => {
  return(
    <React.Fragment>
      <Modal
        isOpen={showForm}
        toggle={handleShowForm}
        >
        <ModalHeader
          toggle={handleShowForm}
          // style={{"backgroundColor": "black", "color": "white"}}
        >
          Add Class
        </ModalHeader>
        <ModalBody>
          <FormInput 
            label={"Class Name"}
            type={"text"}
            name={"className"}
            placeholder={"Enter Class Name"}
            required={props.classNameRequired}
            onChange={handleClassName}
          />
          <FormInput 
            label={"Instructor"}
            type={"text"}
            name={"instructor"}
            placeholder={"Enter Instructor Name"}
            required={props.instructorRequired}
            onChange={handelInstructor}
          />
          <FormInput 
            label={"Schedule"}
            type={"text"}
            name={"schedule"}
            placeholder={"Enter Class Schedule"}
            required={props.scheduleRequired}
            onChange={handleClassSchedule}
          />
          <FormInput 
            label={"Time"}
            type={"text"}
            name={"time"}
            placeholder={"Enter Time Slot"}
            required={props.timeRequired}
            onChange={handleClassTime}
          />
          <FormInput 
            label={"Rate"}
            type={"number"}
            name={"rate"}
            placeholder={"Enter Class Rate"}
            required={props.rateRequired}
            onChange={handleClassRate}
          />
          <input
            className="form-control"
            type="file"
            accept="image/*"
            onChange={(e) => props.handleClassImageChange(e)}
          />
          <br />
          <Button
            block
            onClick={props.handleSave}
          >Add Class</Button>
        </ModalBody>
      </Modal>
    </React.Fragment>
  )
}

export default ClassFrom;