import React, {useState, useEffect} from 'react';
import {
  Card, CardText, CardBody,
  CardTitle, Button, Col,CardImg
} from 'reactstrap';
import BookingMultiDays from './BookingMultiDays';
import Axios from 'axios';



const ClassCard = (props) => {
  const [showBooking, setShowBooking] = useState(false)
  const [user, setUser] = useState("")
  const [bookedDates, setBookdates] = useState([]);

  const classes = props.indivClass
  

  useEffect(() => {
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])
  
  const handleBooking = () => {
    setShowBooking(!showBooking)

    Axios.get('https://radiant-forest-82895.herokuapp.com/showbookingsbyuserperclass/'+ user.id + '/' + classes._id)
      .then(res=>{
        setBookdates(res.data)
      })
  }

  return (
    <React.Fragment>
      <Col lg={4} md={6}>
        <Card className="mb-4">
          <CardImg top width="100%" src={'https://radiant-forest-82895.herokuapp.com/'+ classes.image} alt="class image" />
          <CardBody>
            <h1 className="text-center">{classes.className}</h1>
            <CardTitle>Instructor: {classes.instructor}</CardTitle>
            <CardText>Schedule: {classes.schedule}</CardText>
            <CardText>Time: {classes.time}</CardText>
            <CardText>Rate: {classes.rate}</CardText>
            {user.isAdmin ? 
              <div>
                <Button
                  block
                  color="danger"
                  onClick={() => props.handleDeleteClass(classes._id)}
                >Delete</Button> 
              </div>
              : 
              <Button
                block
                onClick={handleBooking}
              >Book Now</Button>
            }
          </CardBody>
        </Card>
      </Col>
      <BookingMultiDays
        handleBooking={handleBooking}
        showBooking={showBooking}
        classes={classes}
        user={user}
        bookedDates={bookedDates}
      />
    </React.Fragment>
  )
}

export default ClassCard;