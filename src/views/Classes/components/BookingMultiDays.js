import React from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import { 
  Modal, 
  ModalHeader, 
  ModalBody,
  Label,
} from "reactstrap";
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from '../../Payments/CheckoutForm';
import moment from 'moment';

export default class BookingMultiDays extends React.Component {
  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.state = {
      selectedDays: [],
      totalAmount: 0,
    };
  }

  handleDayClick(day, { selected }) {
    const { selectedDays } = this.state;
    if (selected) {
      const selectedIndex = selectedDays.findIndex(selectedDay =>
        DateUtils.isSameDay(selectedDay, day)
      );
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
    }
    this.setState({ selectedDays });
  
    this.setState({totalAmount: selectedDays.length * this.props.classes.rate})
    
  }


  toggle = () => {
    this.setState({dropdownOpen: !this.state.dropdownOpen});
  }

  render() {
    const disabledDays = {
      daysOfWeek: [0, 6]
    }

    // booked dates by user per class
    const todisabledDays = this.props.bookedDates

    let mappedDays = []

    todisabledDays.map(disableDays=>{
      return mappedDays.push(...disableDays.dates)
    })
    
    let allDisabledDays = mappedDays.map(allDisableDay=>{
      return new Date(allDisableDay)
    })
    allDisabledDays.push(disabledDays)

    // to disable previous dates
    const today = [new Date()];
    let b = today.map(dd=>{
      return {
        before : new Date(moment(dd).format("MM/DD/YY")),
        after: new Date(moment(dd).add(90, "days").format("MM/DD/YY"))
      }
    })
    allDisabledDays.unshift(b[0])

    let days = this.state.selectedDays.map(selectedDay=>moment(selectedDay).format("MM/DD/YY"))

    return (
      <div>
        <Modal
        isOpen={this.props.showBooking}
        toggle={this.props.handleBooking}
        >
          <ModalHeader
            toggle={this.props.handleBooking}
          >
            Book Class
          </ModalHeader>
          <ModalBody>
            <h1 className="text-center">{this.props.classes.className}</h1>
              <Label>Choose Date(s):</Label>
              <br />
              <div className="text-center">
                <DayPicker
                  showOutsideDays
                  selectedDays={this.state.selectedDays}
                  onDayClick={this.handleDayClick}
                  disabledDays={allDisabledDays}
                />
              </div>
              <StripeProvider apiKey="pk_test_INICcUua8S7Y2IByi4LIx11y00B4YtiL3O">
              <div className="example">
                <Elements>
                  <CheckoutForm 
                    totalAmount={this.state.totalAmount}
                    cName={this.props.classes.className}
                    cId={this.props.classes._id}
                    handleSaveBooking={this.props.handleSaveBooking}
                    selectedDays={days}
                    user={this.props.user.name}
                    userId={this.props.user.id}
                  />
                </Elements>
              </div>
            </StripeProvider>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}