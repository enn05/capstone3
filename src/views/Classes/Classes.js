import React, {useState, useEffect} from 'react';
import Navbar from '../Layout/Navbar';
import ClassCard from './components/ClassCard';
import ClassForm from './components/ClassForm';
import Axios from 'axios';
import Swal from 'sweetalert2';

const Classes = () => {
  const [classes, setClasses] = useState([]);
  const [user, setUser] = useState("")
  const [showForm, setShowForm] = useState(false);
  const [className, setClassName] = useState("")
  const [instructor, setInstructor] = useState("")
  const [schedule, setSchedule] = useState("")
  const [time, setTime] = useState("")
  const [rate, setRate] = useState("")
  const [image, setImage] = useState({})

  useEffect(()=>{
    fetch('https://radiant-forest-82895.herokuapp.com/showclasses')
      .then(res => res.json())
      .then(data => {
        setClasses(data)
      });
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])

  const handleRefresh = () => {
    setShowForm(false)
    setClassName("")
    setInstructor("")
    setSchedule("")
    setTime("")
    setRate("")
    setImage("")
  }

  const handleShowForm = () => {
    setShowForm(!showForm)
  }

  const handleClassName = (e) => {
    setClassName(e.target.value)
  }

  const handelInstructor = (e) => {
    setInstructor(e.target.value)
  }

  const handleClassSchedule = (e) => {
    setSchedule(e.target.value)
  }

  const handleClassTime = (e) => {
    setTime(e.target.value)
  }

  const handleClassRate = (e) => {
    setRate(e.target.value)
  }

  const handleClassImageChange = (e) => {
    setImage(e.target.files.item(0));
  }

  const handleSave = () => {
    const formData = new FormData();
    formData.append('genre', className)
    formData.append('instructor', instructor)
    formData.append('schedule', schedule)
    formData.append('time', time)
    formData.append('rate', rate)
    formData.append('image', image,image.name)
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    try{
      Axios.post('https://radiant-forest-82895.herokuapp.com/addclass', formData,config)
        .then(res=>{
          let newClasses = [...classes]
          newClasses.push(res.data)
          setClasses(newClasses)
          console.log(formData)
          handleRefresh()
          Swal.fire(
            'Success!',
            'Class added successfully',
            'success'
          )
        })
    }catch(e){
      console.log(e)
    }
  }

  const handleDeleteClass = (classId) => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        Axios({
          method: "DELETE",
          url: "https://radiant-forest-82895.herokuapp.com/deleteclass/" + classId
        }).then(res=>{
          let newClasses = classes.filter(indivClass=>indivClass._id!==classId)
          setClasses(newClasses)
        })
      }
    })
  }

  return(
    <React.Fragment>
    <Navbar />
      <div className="pt-1">
        <h1 className="text-center pt-5 pb-3" id="border-design">Classes</h1>
          <div className="row">
            <div className="col-4 offset-8 text-right">
            
              <ClassForm 
                showForm={showForm}
                handleShowForm={handleShowForm}
                handleClassName={handleClassName}
                handelInstructor={handelInstructor}
                handleClassSchedule={handleClassSchedule}
                handleClassTime={handleClassTime}
                handleClassRate={handleClassRate}
                handleClassImageChange={handleClassImageChange}
                className={className}
                schedule={schedule}
                instructor={instructor}
                time={time}
                rate={rate}
                handleSave={handleSave}
              />
            </div>
          </div>
        <div className="container">
          <div className="row justify-content-center">
              <div className="col-md-12 ">
                <div className="text-right pb-1">
                  {user.isAdmin ?
                    <button className="btn btn-secondary"
                      onClick={handleShowForm}
                    >Add Class</button>
                    :
                    ""
                  }
                </div>
                <div className="row">
                  {classes.map(indivClass=>
                    <ClassCard
                      key={indivClass._id}
                      indivClass={indivClass}
                      user={user}
                      handleDeleteClass={handleDeleteClass}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
    </React.Fragment>
  )
}

export default Classes;


