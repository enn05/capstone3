import React from 'react';
import {
  FormGroup,
  Label,
  Input
} from 'reactstrap';

const FormInput = ({name, label, type, placeholder, onChange, defaultValue, required, onBlur,notMatched, ...props}) => {
  return(
    <React.Fragment>
      <FormGroup>
        <Label>{label}</Label>
        <Input 
          name={name}
          type={type}
          placeholder={placeholder}
          onChange={onChange}
          defaultValue={defaultValue}
          onBlur={onBlur}
          style={ 
            required 
            ? {border: 'solid 1px red' } 
            : 
            notMatched ?
            {border: 'solid 1px red' } 
            :
            null}
        />
      </FormGroup>
    </React.Fragment>
  )
}

export default FormInput;